using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using PlusMinus;

namespace PlusMinusTest;

public class PlusMinusTests 
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void Test1()
    {
        int[] arr = new []{ 1, 1, 0, -1, -1 };
        List<string> res = Result.plusMinus(arr.ToList<int>());

        Assert.AreEqual("0.400000", res[0].ToString());
        Assert.AreEqual("0.400000", res[1]);
        Assert.AreEqual("0.200000", res[2]);
    }
}
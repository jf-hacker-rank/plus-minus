﻿using System;

/*
 * Given an array of integers, calculate the ratios of its elements that are positive, negative, and zero. Print the decimal value of each fraction on a new line with
   places after the decimal.
   
   Note: This challenge introduces precision problems. The test cases are scaled to six decimal places, though answers with absolute error of up to
   are acceptable.
   
   https://www.hackerrank.com/challenges/three-month-preparation-kit-plus-minus/problem?isFullScreen=true&h_l=interview&playlist_slugs%5B%5D=preparation-kits&playlist_slugs%5B%5D=three-month-preparation-kit&playlist_slugs%5B%5D=three-month-week-one
 */
namespace PlusMinus
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine().Trim());

            List<int> arr = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arrTemp => Convert.ToInt32(arrTemp)).ToList();

            List<string> res = Result.plusMinus(arr);
            foreach (string item in res)
            {
                Console.WriteLine(item);
            }
        }
    }
}
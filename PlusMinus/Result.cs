using System.Globalization;

namespace PlusMinus;

public class Result
{

    /*
     * Complete the 'plusMinus' function below.
     *
     * The function accepts INTEGER_ARRAY arr as parameter.
     */
    public static string RoundDecimalToString(decimal value)
    {
        return string.Format("{0:N6}", value);
    }
    public static List<string> plusMinus(List<int> arr)
    {
        List<string> results = new List<string>();

        int numZero = 0;
        int numPos = 0;
        int numNeg = 0;
            
        foreach (int item in arr)
        {
            if(item == 0)
            {
                numZero += 1;
            }

            if (item > 0)
            {
                numPos += 1;
            }

            if (item < 0)
            {
                numNeg += 1;
            }
        }
            
        // pos
        if (numPos == 0)
        {
            results.Add("0.00000");
        }
        else
        {
            results.Add(RoundDecimalToString((decimal)numPos / arr.Count));
        }
        // neg
        if (numNeg == 0)
        {
            results.Add("0.00000");
        }
        else
        {
            results.Add(RoundDecimalToString((decimal)numNeg/ arr.Count));
        }
        // zero
        if (numZero== 0)
        {
            results.Add("0.00000");
        }
        else
        {
            results.Add(RoundDecimalToString((decimal)numZero / arr.Count));
        }

        return results;

    }

}